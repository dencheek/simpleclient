package com.simpleclient.db;

import com.simpleclient.models.Commit;
import com.simpleclient.models.GitHubToken;
import com.simpleclient.models.Repository;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmHelper {

    private static final String ENCRIPTION_KEY = "CVPdQNAT6fBI4rrPLEn9FDLFNI7Bqe6bLFBFUsnfx0+UV84DoqLFiNHpKOPLRW0=";

    public static RealmConfiguration getEncriptedConfiguration() {
        return new RealmConfiguration.Builder()
                .encryptionKey(ENCRIPTION_KEY.getBytes())
                .build();
    }

    public static void saveToken(Realm realm, String token) {
        realm.executeTransaction(r -> {
            GitHubToken realmObject = r.createObject(GitHubToken.class);
            realmObject.setAccessToken(token);
        });
    }

    public static String getToken(Realm realm) {
        GitHubToken token = realm.where(GitHubToken.class).findFirst();
        String result = (token == null) ? null : token.getAccessToken();
        return result;
    }

    public static Realm saveRepositories(Realm realm, List<Repository> repositories) {
        realm.executeTransactionAsync(r -> {
            r.copyToRealmOrUpdate(repositories);
        });
        return realm;
    }

    public static Repository getRepository(Realm realm, int position) {
        Repository repository = realm.where(Repository.class).findAll().get(position);
        return repository;
    }

    public static void saveCommits(Realm realm, List<Commit> commits) {
        realm.executeTransactionAsync(r -> {
            r.copyToRealmOrUpdate(commits);
        });
    }

    public static void clearCommits(Realm realm) {
        realm.executeTransaction(r -> {
            r.delete(Commit.class);
        });
    }

    public static void clearAll(Realm realm) {
        realm.executeTransaction(r -> {
            r.delete(GitHubToken.class);
        });
    }
}