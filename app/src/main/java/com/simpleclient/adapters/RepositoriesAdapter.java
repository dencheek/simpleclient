package com.simpleclient.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.simpleclient.R;
import com.simpleclient.models.Repository;
import com.squareup.picasso.Picasso;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class RepositoriesAdapter extends RealmRecyclerViewAdapter<Repository, RepositoriesAdapter.RepositoryViewHolder> {

    private View.OnClickListener listener;

    public RepositoriesAdapter(@Nullable OrderedRealmCollection<Repository> data, boolean autoUpdate, View.OnClickListener listener) {
        super(data, autoUpdate);
        this.listener = listener;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo, parent, false);
        itemView.setOnClickListener(listener);
        return new RepositoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        Repository repository = getItem(position);
        holder.tvRepoName.setText(String.format("%1s/%2s", repository.user.name, repository.name));
        holder.tvDescription.setText(repository.description);
        holder.tvCounter.setText(String.format("%d/%d", repository.forksCount, repository.watchersCount));
        Picasso.with(holder.ivAvatar.getContext())
                .load(repository.user.avatarUrl)
                .fit().centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.ivAvatar);
    }

    class RepositoryViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivAvatar;
        private TextView tvRepoName;
        private TextView tvDescription;
        private TextView tvCounter;

        public RepositoryViewHolder(View itemView) {
            super(itemView);
            ivAvatar = (ImageView) itemView.findViewById(R.id.ivAvatar);
            tvRepoName = (TextView) itemView.findViewById(R.id.tvName);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvCounter = (TextView) itemView.findViewById(R.id.tvCounter);
        }
    }
}
