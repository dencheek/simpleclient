package com.simpleclient.adapters;


import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.simpleclient.R;
import com.simpleclient.models.Commit;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class CommitsAdapter extends RealmRecyclerViewAdapter<Commit, CommitsAdapter.CommitViewHolder> {


    public CommitsAdapter(@Nullable OrderedRealmCollection<Commit> data, boolean autoUpdate) {
        super(data, autoUpdate);
    }

    @Override
    public CommitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_commit, parent, false);
        return new CommitViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommitViewHolder holder, int position) {
        Commit commit = getItem(position);
        holder.tvMessage.setText(String.format("%1s : %2s", commit.author, commit.message));
        holder.tvHash.setText(commit.sha);
        holder.tvDate.setText(formatDate(commit.date));
    }

    private String formatDate(String date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withLocale(Locale.UK);
        DateTime dt = formatter.parseDateTime(date);
        return dt.toString(DateTimeFormat.shortDate());
    }

    class CommitViewHolder extends RecyclerView.ViewHolder {
        private TextView tvHash;
        private TextView tvMessage;
        private TextView tvDate;


        public CommitViewHolder(View itemView) {
            super(itemView);
            tvHash = (TextView) itemView.findViewById(R.id.tvHash);
            tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
        }
    }
}
