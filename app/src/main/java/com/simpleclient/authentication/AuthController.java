package com.simpleclient.authentication;

import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.afollestad.materialdialogs.MaterialDialog;

import okhttp3.HttpUrl;

public class AuthController implements AuthWebViewClient.UrlChangeHandler {

    public static final String OAUTH_HOST = "www.github.com";
    public static final String CLIENT_ID = "5e7d290bd2f2ae0fe436";
    public static final String CLIENT_SECRET = "5a37f7498db3c42edb70bd0727032b50836ce014";
    public static final String INITIAL_SCOPE = "user,public_repo,repo";
    public static final String REDIRECT_URL = "https://finished";

    RedirectListener redirectListener;
    WebView webView;

    public AuthController(WebView webView, RedirectListener authHandler) {
        this.redirectListener = authHandler;
        this.webView = webView;
    }

    public void authenticate(MaterialDialog progressDialog) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setAppCacheEnabled(false);
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new AuthWebViewClient(progressDialog, this));
        HttpUrl.Builder url = new HttpUrl.Builder()
                .scheme("https")
                .host(OAUTH_HOST)
                .addPathSegment("login")
                .addPathSegment("oauth")
                .addPathSegment("authorize")
                .addQueryParameter("client_id", CLIENT_ID)
                .addQueryParameter("scope", INITIAL_SCOPE);

        webView.loadUrl(url.toString());
    }

    @Override
    public void onUrlChange(String url) {
        if (url.startsWith(REDIRECT_URL)) {
            webView.setVisibility(View.GONE);
            redirectListener.onRedirected(url);
        }
    }

    public interface RedirectListener {

        void onRedirected(String url);
    }
}
