package com.simpleclient.authentication;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.afollestad.materialdialogs.MaterialDialog;
import com.simpleclient.R;

public class AuthWebViewClient extends WebViewClient {

    private UrlChangeHandler urlChangeHandler;
    private MaterialDialog progressDialog;

    public AuthWebViewClient(MaterialDialog progressDialog, UrlChangeHandler urlChangeHandler) {
        this.progressDialog = progressDialog;
        this.urlChangeHandler = urlChangeHandler;
    }

    @Override
    public void onPageStarted(android.webkit.WebView view, String url, Bitmap favicon) {
        this.progressDialog.setContent(R.string.login_activity_authenticating);
        progressDialog.show();
    }

    @Override
    public void onPageFinished(android.webkit.WebView view, String url) {
        view.clearCache(true);
        view.clearHistory();
        progressDialog.dismiss();
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return loadUrl(view, url);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        return loadUrl(view, request.getUrl().toString());
    }

    private boolean loadUrl(WebView webView, String url) {
        webView.loadUrl(url);
        urlChangeHandler.onUrlChange(url);
        return false;
    }

    public interface UrlChangeHandler {
        void onUrlChange(String url);
    }
}
