package com.simpleclient.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Repository extends RealmObject implements Parcelable {

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }

        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
    @PrimaryKey
    @SerializedName("id")
    public int id;
    @SerializedName("owner")
    public User user;
    @SerializedName("name")
    public String name;
    @SerializedName("full_name")
    public String fullName;
    @SerializedName("forks_count")
    public int forksCount;
    @SerializedName("watchers_count")
    public int watchersCount;
    @SerializedName("description")
    public String description;

    public Repository() {
    }

    protected Repository(Parcel in) {
        id = in.readInt();
        user = in.readParcelable(User.class.getClassLoader());
        name = in.readString();
        fullName = in.readString();
        forksCount = in.readInt();
        watchersCount = in.readInt();
        description = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeParcelable(user, flags);
        dest.writeString(name);
        dest.writeString(fullName);
        dest.writeInt(forksCount);
        dest.writeInt(watchersCount);
        dest.writeString(description);
    }
}
