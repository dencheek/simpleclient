package com.simpleclient.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Commit extends RealmObject {

    @PrimaryKey
    @SerializedName("sha")
    public String sha;

    @SerializedName("message")
    public String message;

    @SerializedName("date")
    public String date;

    @SerializedName("login")
    public String author;
}
