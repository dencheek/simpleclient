package com.simpleclient.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RequestToken implements Parcelable {

    public static final Creator<RequestToken> CREATOR = new Creator<RequestToken>() {
        @Override
        public RequestToken createFromParcel(Parcel in) {
            return new RequestToken(in);
        }

        @Override
        public RequestToken[] newArray(int size) {
            return new RequestToken[size];
        }
    };
    @SerializedName("client_id")
    private String clientId;
    @SerializedName("client_secret")
    private String clientSecret;
    @SerializedName("redirect_uri")
    private String redirectUri;
    @SerializedName("code")
    private String code;

    public RequestToken(String clientId, String clientSecret, String redirectUri, String code) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.redirectUri = redirectUri;
        this.code = code;
    }

    protected RequestToken(Parcel in) {
        clientId = in.readString();
        clientSecret = in.readString();
        redirectUri = in.readString();
        code = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(clientId);
        dest.writeString(clientSecret);
        dest.writeString(redirectUri);
        dest.writeString(code);
    }
}
