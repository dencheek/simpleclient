package com.simpleclient.models.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.simpleclient.models.Commit;

import java.lang.reflect.Type;

public class CommitDeserializer implements JsonDeserializer<Commit> {
    @Override
    public Commit deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Commit commit = new Commit();
        if (json.isJsonObject()) {
            commit.sha = json.getAsJsonObject().get("sha").getAsString();
            JsonElement commitJsonElement = json.getAsJsonObject().get("commit");
            if (commitJsonElement != null && commitJsonElement.isJsonObject()) {
                JsonElement committerJsonElement = commitJsonElement.getAsJsonObject().get("committer");
                if (committerJsonElement != null && committerJsonElement.isJsonObject()) {
                    commit.date = committerJsonElement.getAsJsonObject().get("date").getAsString();
                }
            }
            commit.message = commitJsonElement.getAsJsonObject().get("message").getAsString();
            JsonElement authorJsonElement = json.getAsJsonObject().get("author");
            if (authorJsonElement != null && authorJsonElement.isJsonObject()) {
                commit.author = authorJsonElement.getAsJsonObject().get("login").getAsString();
            }
        }

        return commit;
    }
}
