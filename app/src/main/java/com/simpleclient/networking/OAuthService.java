package com.simpleclient.networking;

import com.simpleclient.models.GitHubToken;
import com.simpleclient.models.RequestToken;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface OAuthService {

    @POST("login/oauth/access_token")
    @Headers("Accept: application/json")
    Observable<Response<GitHubToken>> getToken(@Body RequestToken body);
}