package com.simpleclient.networking;

import com.simpleclient.models.Commit;
import com.simpleclient.models.Repository;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RepositoryService {

    @GET("/user/repos")
    Observable<List<Repository>> getUserRepositories();

    @GET("repos/{owner}/{repo}/commits")
    Observable<List<Commit>> getCommits(@Path("owner") String owner, @Path("repo") String repo);
}
