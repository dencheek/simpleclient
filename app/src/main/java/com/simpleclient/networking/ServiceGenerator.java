package com.simpleclient.networking;


import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.simpleclient.db.RealmHelper;
import com.simpleclient.models.Commit;
import com.simpleclient.models.deserializers.CommitDeserializer;
import com.simpleclient.models.exceptions.RxErrorHandlingCallAdapterFactory;

import java.io.IOException;

import io.realm.Realm;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private final static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY);

    private final static Retrofit.Builder builder = new Retrofit.Builder()
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(getGson()));

    private final static OkHttpClient httpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build();

    private static Gson getGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Commit.class, new CommitDeserializer());
        return gsonBuilder.create();
    }

    public static <S> S createService(Class<S> serviceClass) {
        OkHttpClient client = httpClient.newBuilder()
                .addInterceptor(new AccessTokenInterceptor())
                .build();

        Retrofit retrofit = builder.baseUrl("https://api.github.com")
                .client(client)
                .build();
        return retrofit.create(serviceClass);
    }

    /**
     * Only used for OAuthService.getToken
     *
     * @return
     */
    public static OAuthService createAuthService() {
        Retrofit retrofit = builder.baseUrl("https://github.com")
                .client(httpClient)
                .build();

        return retrofit.create(OAuthService.class);
    }

    private static class AccessTokenInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            String[] headers = {
                    "application/vnd.github.html+json",
                    "application/vnd.github.raw+json"
            };
            Realm realm = Realm.getInstance(RealmHelper.getEncriptedConfiguration());
            String token = RealmHelper.getToken(realm);
            Request.Builder requestBuilder = original.newBuilder()
                    .header("Authorization", "Token " + token)
                    .method(original.method(), original.body());

            if (original.header("Accept") == null) {
                requestBuilder.addHeader("Accept", TextUtils.join(",", headers));
            }

            Request request = requestBuilder.build();
            return chain.proceed(request);
        }
    }

}
