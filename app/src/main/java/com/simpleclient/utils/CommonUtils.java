package com.simpleclient.utils;

import android.content.Context;
import android.widget.Toast;

import com.simpleclient.models.exceptions.RetrofitException;

public class CommonUtils {

    public static void showError(Context context, Throwable throwable) {
        if (throwable instanceof RetrofitException) {
            RetrofitException exception = (RetrofitException) throwable;
            String errorMessage = String.format("%1s : %2s", exception.getResponse().code(), exception.getResponse().message());
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }
}