package com.simpleclient.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.transition.Visibility;
import android.view.View;

import com.simpleclient.R;
import com.simpleclient.adapters.RepositoriesAdapter;
import com.simpleclient.db.RealmHelper;
import com.simpleclient.models.Repository;
import com.simpleclient.networking.RepositoryService;
import com.simpleclient.networking.ServiceGenerator;
import com.simpleclient.utils.CommonUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private RepositoriesAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        setUpRecyclerView();
        init(true);
    }

    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        adapter = new RepositoriesAdapter(realm.where(Repository.class).findAllAsync(), true, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(() -> {
            init(false);
        });
    }

    private void init(boolean showProgress) {
        if (showProgress) {
            progressDialog.setContent(getString(R.string.loading_repositories));
            progressDialog.show();
        }

        ServiceGenerator.createService(RepositoryService.class).getUserRepositories()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .compose(this.bindToLifecycle())
                .doOnNext(repositories -> {
                    Realm backgroundRealm = Realm.getInstance(RealmHelper.getEncriptedConfiguration());
                    RealmHelper.saveRepositories(backgroundRealm, repositories);
                    backgroundRealm.close();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(n -> {
                    dismissProgressViews();
                }, error -> {
                    dismissProgressViews();
                    CommonUtils.showError(MainActivity.this, error);
                });
    }

    private void dismissProgressViews() {
        progressDialog.dismiss();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onClick(View v) {
        int itemPosition = recyclerView.getChildLayoutPosition(v);
        Repository repository = RealmHelper.getRepository(realm, itemPosition);
        Intent i = new Intent(this, DetailsActivity.class);
        i.putExtra(DetailsActivity.REPOSITORY_NAME, repository.name);
        i.putExtra(DetailsActivity.OWNER_NAME, repository.user.name);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            transitionTo(i);
        } else {
            startActivity(i);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected Visibility buildEnterTransition() {
        Fade enterTransition = new Fade();
        enterTransition.setDuration(500);
        return enterTransition;
    }
}
