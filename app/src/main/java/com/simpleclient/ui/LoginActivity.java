package com.simpleclient.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.simpleclient.R;
import com.simpleclient.authentication.AuthController;
import com.simpleclient.db.RealmHelper;
import com.simpleclient.models.GitHubToken;
import com.simpleclient.models.RequestToken;
import com.simpleclient.networking.ServiceGenerator;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

import static com.simpleclient.authentication.AuthController.CLIENT_ID;
import static com.simpleclient.authentication.AuthController.CLIENT_SECRET;
import static com.simpleclient.authentication.AuthController.REDIRECT_URL;

public class LoginActivity extends RxAppCompatActivity implements AuthController.RedirectListener {

    private MaterialDialog progressDialog;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        realm = Realm.getInstance(RealmHelper.getEncriptedConfiguration());
        progressDialog = new MaterialDialog.Builder(this)
                .progress(true, 0).build();
        WebView webView = (WebView) findViewById(R.id.login_webview);
        String token = RealmHelper.getToken(realm);
        if (token != null) {
            openMain();
        } else {
            AuthController authController = new AuthController(webView, this);
            authController.authenticate(progressDialog);
        }
    }

    @Override
    public void onRedirected(String url) {
        login(Uri.parse(url));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Uri uri = intent.getData();
        login(uri);
    }

    private void login(Uri uri) {
        if (uri != null) {
            progressDialog.setContent(getString(R.string.loading_user));
            progressDialog.show();
            String code = uri.getQueryParameter("code");
            RequestToken requestToken = new RequestToken(
                    CLIENT_ID,
                    CLIENT_SECRET,
                    REDIRECT_URL,
                    code
            );

            ServiceGenerator.createAuthService()
                    .getToken(requestToken)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(this.bindToLifecycle())
                    .subscribe(response -> {
                        GitHubToken token = response.body();
                        if (token.getAccessToken() != null) {

                            RealmHelper.saveToken(realm, token.getAccessToken());
                            progressDialog.dismiss();
                            openMain();

                        } else if (token.getError() != null) {
                            Toast.makeText(this, token.getError(), Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    }, Throwable::printStackTrace);
        }
    }

    private void openMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }
}
