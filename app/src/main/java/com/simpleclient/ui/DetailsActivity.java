package com.simpleclient.ui;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.transition.Visibility;

import com.simpleclient.R;
import com.simpleclient.adapters.CommitsAdapter;
import com.simpleclient.db.RealmHelper;
import com.simpleclient.models.Commit;
import com.simpleclient.networking.RepositoryService;
import com.simpleclient.networking.ServiceGenerator;
import com.simpleclient.utils.CommonUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

public class DetailsActivity extends BaseActivity {

    public static final String OWNER_NAME = "ownerName";
    public static final String REPOSITORY_NAME = "repoName";

    private RecyclerView recyclerView;
    private CommitsAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        setupActionBar();
        setUpRecyclerView();
        init(true);
    }

    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        adapter = new CommitsAdapter(realm.where(Commit.class).findAllAsync(), true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(DetailsActivity.this, R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(() -> {
            init(false);
        });
    }

    private void init(boolean showProgress) {
        String repoName = getIntent().getStringExtra(REPOSITORY_NAME);
        String ownerName = getIntent().getStringExtra(OWNER_NAME);
        if (showProgress) {
            progressDialog.setContent(getString(R.string.loading_commits));
            progressDialog.show();
        }
        ServiceGenerator.createService(RepositoryService.class).getCommits(ownerName, repoName)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .compose(this.bindToLifecycle())
                .doOnNext(commits -> {
                    Realm backgroundRealm = Realm.getInstance(RealmHelper.getEncriptedConfiguration());
                    RealmHelper.saveCommits(backgroundRealm, commits);
                    backgroundRealm.close();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(n -> {
                    dismissProgressViews();
                }, throwable -> {
                    dismissProgressViews();
                    CommonUtils.showError(DetailsActivity.this, throwable);
                });
    }

    private void dismissProgressViews() {
        progressDialog.dismiss();
        swipeRefreshLayout.setRefreshing(false);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected Visibility buildEnterTransition() {
        Explode enterTransition = new Explode();
        enterTransition.setDuration(500);
        return enterTransition;
    }
}
